import 'package:flutter/material.dart';
import 'package:gallery/util/motivation_tile.dart';

class MotivationPage extends StatefulWidget {
  @override
  State<MotivationPage> createState() => _MotivationPageState();
}

class _MotivationPageState extends State<MotivationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        title: Text(
          'MOTIVATION',
          style: TextStyle(
            color: Colors.tealAccent[700],
            fontSize: 25,
            fontWeight: FontWeight.bold,
            letterSpacing: 2.0,
          ),
        ),
        backgroundColor: Colors.grey[900],
        elevation: 0,
        centerTitle: true,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
              child: Text(
                'Exercises and Meditation is the key to Healthy and Happy life',
                style: TextStyle(
                  color: Colors.grey[400],
                  fontSize: 35,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 2.0,
                ),
              ),
            ),

            // height diff
            SizedBox(
              height: 25,
            ),

            // expanded widget, curved, wrap all list tiles
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black87,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(20.0),
                  ),
                ),
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
                child: ListView(
                  scrollDirection: Axis.vertical,
                  children: [
                    MotivationTile(
                      image: CircleAvatar(
                        child: Icon(
                          Icons.self_improvement_rounded,
                          color: Colors.grey[900],
                        ),
                        backgroundColor: Colors.tealAccent[400],
                      ),
                      author: 'Bruce Lee',
                      motivation:
                          'One kick 10 000 times is better than 10 000kicks at once.',
                    ),
                    SizedBox(height: 15.0),
                    MotivationTile(
                      image: CircleAvatar(
                        child: Icon(
                          Icons.food_bank_rounded,
                          color: Colors.grey[900],
                        ),
                        backgroundColor: Colors.tealAccent[400],
                      ),
                      author: 'Warren Buffet',
                      motivation: 'Success is hard, but worth it.',
                    ),
                    SizedBox(height: 15.0),
                    MotivationTile(
                      image: CircleAvatar(
                        child: Icon(
                          Icons.rocket_rounded,
                          color: Colors.grey[900],
                        ),
                        backgroundColor: Colors.tealAccent[400],
                      ),
                      author: 'Elon Musk',
                      motivation:
                          'Rather be an optimistic and wrong than pessimistic and right.',
                    ),
                    SizedBox(height: 15.0),
                    MotivationTile(
                      image: CircleAvatar(
                        child: Icon(
                          Icons.rocket_launch_rounded,
                          color: Colors.grey[900],
                        ),
                        backgroundColor: Colors.tealAccent[400],
                      ),
                      author: 'Elon Musk',
                      motivation: 'If you need inspiration, do not do it.',
                    ),
                    SizedBox(height: 15.0),
                    MotivationTile(
                      image: CircleAvatar(
                        child: Icon(
                          Icons.person_pin_rounded,
                          color: Colors.grey[900],
                        ),
                        backgroundColor: Colors.tealAccent[400],
                      ),
                      author: 'Nelson Mandela',
                      motivation: 'A winner is a dreamer who never gives up.',
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
