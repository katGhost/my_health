import 'package:flutter/material.dart';
import 'package:gallery/util/exercises_list.dart';

class Exercises extends StatefulWidget {
  // const Exercises({Key? key}) : super(key: key);

  @override
  State<Exercises> createState() => _ExercisesState();
}

class _ExercisesState extends State<Exercises> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        title: Text(
          'Exercises',
          style: TextStyle(
              color: Colors.tealAccent, fontSize: 25, letterSpacing: 2.0),
        ),
        centerTitle: true,
        backgroundColor: Colors.grey[900],
        elevation: 0,
      ),
      body: SafeArea(
        child: Column(
          // choose the best exercise for the day
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Text('Choose Your Best Suited Exercise',
                  style: TextStyle(
                      color: Colors.grey, fontSize: 50, letterSpacing: 2.0)),
            ),
            SizedBox(
              height: 25,
            ),
            // Main center information
            Container(
              padding: EdgeInsets.symmetric(horizontal: 25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Colors.teal.shade400,
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    child: Text(
                      'New Activities for you',
                      style: TextStyle(
                        color: Colors.grey[900],
                        fontSize: 18,
                        letterSpacing: 1.0,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Colors.teal.shade400,
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    child: Text(
                      'See all',
                      style: TextStyle(
                        color: Colors.grey[900],
                        fontSize: 18,
                        letterSpacing: 1.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(
              height: 25,
            ),
            Expanded(
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  ExercisesTile(
                    icon: Icons.run_circle_rounded,
                    title: 'RUNNING',
                  ),
                  ExercisesTile(
                    icon: Icons.snowboarding_rounded,
                    title: 'SNOWBOARDING',
                  ),
                  ExercisesTile(
                    icon: Icons.book_rounded,
                    title: 'READING',
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
