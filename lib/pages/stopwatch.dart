import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gallery/util/button_widget.dart';

class CountDownTimer extends StatefulWidget {
  const CountDownTimer({Key? key}) : super(key: key);
  @override
  State<CountDownTimer> createState() => _CountDownTimerState();
}



class _CountDownTimerState extends State<CountDownTimer> {
  static const stopWatchDuration = Duration(minutes: 0);
  Duration duration = Duration();
  Timer? timer;

  // a boolean to stop the timer
  bool isComplete = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // startCountDown();
    reset();
  }

  void reset() {
    if (isComplete) {
      setState(() {
        duration = stopWatchDuration;
      });
    } else {
      duration = Duration();
    }
  }

  void addTime() {
    final addSeconds = 1;

    setState(() {
      final seconds = duration.inSeconds + addSeconds;

      duration = Duration(seconds: seconds);
    });
  }

  void startCountDown() {
    timer = Timer.periodic(Duration(seconds: 1), (_) => addTime());
  }

  void stopTimer({bool resets = true}) {
    if (resets) {
      reset();
    }

    setState(() {
      timer?.cancel();
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.grey[900],
        appBar: AppBar(
          title: Text(
            'Stopwatch Timer',
            style: TextStyle(
              color: Colors.tealAccent[700],
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
          ),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.grey[900],
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black54,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                padding: EdgeInsets.all(25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Stopwatch',
                      style: TextStyle(
                        color: Colors.grey.shade400,
                        fontWeight: FontWeight.normal,
                        fontSize: 20,
                        letterSpacing: 1.0,
                      ),
                    ),
                    Icon(
                      Icons.timer,
                      color: Colors.tealAccent,
                      size: 50,
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 30.0),
            Center(
              child: Column(
                children: [
                  buildTime(),
                  const SizedBox(
                    height: 60,
                  ),
                  buildButtons(),
                ],
              ),
            ),
          ],
        ),
      );

  Widget buildButtons() {
    final isRunning = timer == null ? false : timer!.isActive;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // import start button from the ButtonWidget file in util
        ButtonWidget(text: 'Start!', onClicked: startCountDown),

        const SizedBox(
          width: 12,
        ),

        // import stop button also from the ButtonWidget file in util
        ButtonWidget(
            text: 'Stop',
            onClicked: () {
              if (isRunning) {
                stopTimer(resets: false);
              }
            })
      ],
    );
  }

  Widget buildTime() {
    // below puts a zero before any number from 0 -> 9 as 01 -> 09
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    final minutes = twoDigits(duration.inMinutes.remainder(60));
    final seconds = twoDigits(duration.inSeconds.remainder(60));

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        buildTimeCard(time: minutes, header: 'MINUTES'),
        const SizedBox(
          width: 8,
        ),
        buildTimeCard(time: seconds, header: 'SECONDS'),
      ],
    );
  }

  Widget buildTimeCard({required String time, required String header}) =>
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              color: Colors.black87,
              borderRadius: BorderRadius.circular(12.0),
            ),
            child: Text(
              time,
              style: TextStyle(
                color: Colors.tealAccent,
                fontWeight: FontWeight.bold,
                fontSize: 72,
              ),
            ),
          ),
          const SizedBox(
            height: 24,
          ),
          Text(
            header,
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.bold,
              fontSize: 20,
              letterSpacing: 1.0,
            ),
          ),
        ],
      );

  /* 
  Text(
      '$minutes:$seconds',
      style: TextStyle(fontSize: 60, color: Colors.tealAccent),
    ); */
}
