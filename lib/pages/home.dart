import 'dart:async';
import 'package:flutter/material.dart';
import 'package:gallery/util/dashboard_tile.dart';

// the dashboard
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],

      // bottom navigation buttons features all screens
      bottomNavigationBar: new Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.grey[900],
          ),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: 0,
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.home,
                    color: Colors.tealAccent[700],
                  ),
                  label: ''),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.widgets_rounded,
                    color: Colors.tealAccent[400],
                  ),
                  label: ''),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.notifications_on,
                    color: Colors.tealAccent[400],
                  ),
                  label: ''),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.photo_library_rounded,
                    color: Colors.tealAccent[400],
                  ),
                  label: ''),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.person,
                    color: Colors.tealAccent[400],
                  ),
                  label: ''),
            ],
          )),

      // Appbar
      appBar: AppBar(
        title: Text(
          'D A S H B O A R D',
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w500,
            fontFamily: 'Anton',
            color: Colors.tealAccent[400],
          ),
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.grey[900],
      ),
      body: SafeArea(
        child: Column(
          children: [
            // Dashboard greetings row
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                // INTRO TO HOME PAGE
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 25.0, vertical: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Hello Again, Welcome!',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.w300,
                          letterSpacing: 2.0,
                        ),
                      ),
                      SizedBox(height: 6.0),
                      Text(
                        'Navigate with ease',
                        style: TextStyle(
                          color: Colors.tealAccent,
                          fontWeight: FontWeight.w100,
                          fontSize: 14,
                          letterSpacing: 1.0,
                        ),
                      ),
                    ],
                  ),
                ),

                // ICON, cool Dashboard UI
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    padding: EdgeInsets.all(12.0),
                    child: Icon(
                      Icons.dashboard_sharp,
                      color: Colors.tealAccent[400],
                    ),
                  ),
                )
              ],
            ),

            // Vertical space between
            SizedBox(height: 20.0),

            // search bar, simple
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: TextField(
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.tealAccent.shade400,
                  ),
                  hintText: 'Find your widgets...',
                  hintStyle: TextStyle(color: Colors.tealAccent.shade400),
                  fillColor: Colors.tealAccent.shade400,
                  focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.tealAccent.shade400)),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.tealAccent.shade400),
                  ),
                ),
              ),
            ),

            SizedBox(height: 25.0),

            // MAIN DASHBOARD CONTENT
            Expanded(
              child: Container(
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  color: Colors.black54,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(20.0),
                  ),
                ),
                child: ListView(
                  scrollDirection: Axis.vertical,
                  children: [
                    // dashboard tile item with app page info and button to that page
                    DashBoardTile(
                      title: 'Login',
                      sub: '10 minutes ago',
                      onClicked: () {
                        setState(() {
                          Navigator.pushNamed(context, '/login');
                        });
                      },
                      color: Colors.deepPurple,
                      icon: Icons.login_rounded,
                    ),
                    // dashboard tile item with app page info and button to that page
                    DashBoardTile(
                      title: 'Exercises',
                      sub: '4 minutes ago',
                      onClicked: () {
                        setState(() {
                          Navigator.pushNamed(context, '/exercise');
                        });
                      },
                      color: Colors.orange,
                      icon: Icons.self_improvement_rounded,
                    ),
                    // dashboard tile item with app page info and button to that page
                    DashBoardTile(
                      title: 'Stopwatch',
                      sub: 'Since yesterday',
                      onClicked: () {
                        setState(() {
                          Navigator.pushNamed(context, '/stopwatch');
                        });
                      },
                      color: Colors.redAccent,
                      icon: Icons.timer_rounded,
                    ),
                    // dashboard tile item with app page info and button to that page
                    DashBoardTile(
                      title: 'Motivation',
                      sub: '25 minutes ago',
                      onClicked: () {
                        setState(() {
                          Navigator.pushNamed(context, '/motivation');
                        });
                      },
                      color: Colors.indigo[700],
                      icon: Icons.lightbulb_rounded,
                    ),
                    // dashboard tile item with app page info and button to that page
                    DashBoardTile(
                      title: 'Profile',
                      sub: '7 minutes ago',
                      onClicked: () {
                        setState(() {
                          Navigator.pushNamed(context, '/profile');
                        });
                      },
                      color: Colors.pink,
                      icon: Icons.person_rounded,
                    ),
                  ],
                ),
              ),
            ),
            // END OF EXPANDED WIDGET
          ],
        ),
      ),
      // REQUIRED FLOATING ACTION BUTTON ->PROFILE EDIT PAGE
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal,
        onPressed: () {
          Navigator.pushNamed(context, '/profile');
        },
        child: Icon(
          Icons.edit,
          color: Colors.white,
        ),
      ),
    );
  }
}
