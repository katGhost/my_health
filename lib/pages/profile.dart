import 'package:flutter/material.dart';
import 'package:gallery/util/profile_tile.dart';

class MyProfile extends StatefulWidget {
  @override
  State<MyProfile> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        title: Text(
          'PROFILE',
          style: TextStyle(
            color: Colors.tealAccent[400],
            fontWeight: FontWeight.bold,
            fontSize: 25,
            letterSpacing: 2.0,
          ),
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.grey[900],
      ),
      // profile body
      body: SafeArea(
        child: Column(
          // mini bar
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Mr Mogashoa, ',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1.0,
                        ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        'Your profile is ready!',
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1.0,
                        ),
                      ),
                    ],
                  ),

                  // profile picture
                  // asset images not working currently unfortunately, rely on icons
                  Container(
                    padding: EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(12.0)),
                    child: Icon(
                      Icons.person,
                      color: Colors.tealAccent[400],
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(
              height: 25,
            ),

            // how u feel card
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Container(
                padding: EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: Row(
                  children: [
                    // animation or cute icon
                    Container(
                      height: 100,
                      width: 100,
                      // color: Colors.red,
                      child: const Icon(
                        Icons.self_improvement_rounded,
                        color: Colors.teal,
                        size: 80,
                      ),
                    ),

                    const SizedBox(width: 25),

                    // how do you feel msg + button
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'How are you?',
                            style: TextStyle(
                              color: Colors.grey[500],
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              letterSpacing: 1.0,
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text(
                            'edit your details in your profile to help us set the activities to your preferred levels',
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              letterSpacing: 1.0,
                            ),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Container(
                            padding: EdgeInsets.all(12.0),
                            decoration: BoxDecoration(
                              color: Colors.teal,
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                            child: Center(
                              child: Text(
                                'Edit Now',
                                style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 1.0,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),

            SizedBox(
              height: 20,
            ),
            // Horizontal view -> all meditation feels
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Check Activity',
                    style: TextStyle(
                      color: Colors.grey[300],
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.0,
                    ),
                  ),
                  Icon(
                    Icons.more_horiz_rounded,
                    color: Colors.grey[300],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            // actual cards
            // use expanded to wrap extended length or hieght items/widgets
            Expanded(
              child: Container(
                // List tiles of aligned horizontally
                // each list with diff content tracking user activity in profile
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    // profile list item
                    ProfileTile(
                      header: 'Progress',
                      icon: Icons.scale_rounded,
                    ),
                    // profile list item
                    ProfileTile(
                      header: 'Car Mode',
                      icon: Icons.drive_eta_rounded,
                    ),
                    // profile list item
                    ProfileTile(
                      header: 'Music',
                      icon: Icons.library_music_rounded,
                    ),
                    // profile list item
                    ProfileTile(
                      header: 'Saved',
                      icon: Icons.download_rounded,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
