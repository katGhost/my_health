import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        title: Text(
          'LOGIN',
          style: TextStyle(
            color: Colors.tealAccent[400],
            fontSize: 30,
            fontWeight: FontWeight.bold,
            letterSpacing: 2.0,
          ),
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.grey[900],
      ),
      // END OF APPBAR
      body: SafeArea(
        // center all of the content going into our safeArea with a Center widget
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.verified_user_sharp,
                size: 80,
                color: Colors.tealAccent[700],
              ),
              SizedBox(height: 25.0),
              // User Hello text
              Text(
                'Please Login!',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Anton',
                  color: Colors.white,
                  fontSize: 36,
                  letterSpacing: 2.0,
                ),
              ),

              SizedBox(height: 10),
              // An intro text or message to the user
              Text(
                'Welcome back, your account is ready!',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey.shade400,
                ),
              ),
              SizedBox(height: 50),

              // User email textfield
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[900],
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    // users username if already a member, logs into acc to a home page
                    child: TextField(
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.person_rounded,
                          color: Colors.teal.shade600,
                        ),
                        hintText: 'Enter your username',
                        hintStyle: TextStyle(color: Colors.teal.shade600),
                        fillColor: Colors.teal.shade600,
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.tealAccent.shade700),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.tealAccent.shade700),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              // END OF USERNAME TEXT FIELD PADDING
              // little height space between the text fields
              SizedBox(height: 10),
              // User password textfield
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[900],
                    borderRadius: BorderRadius.circular(10),
                  ),
                  // wrapping textfield in a padding and give it a box decoration
                  // box decoration controlls the border lines and edges/curves
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    // password for the user goes below
                    // NB* not linked to any database
                    child: TextField(
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.key_outlined,
                          color: Colors.teal.shade600,
                        ),
                        hintText: 'Enter your password',
                        hintStyle: TextStyle(color: Colors.teal.shade600),
                        fillColor: Colors.teal.shade600,
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.tealAccent.shade700),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.tealAccent.shade700),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              // END OF PASSWORD TEXT FIELD PADDING
              SizedBox(height: 25),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Not a member?',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.grey,
                    ),
                  ),
                  Text(
                    'Register Now!',
                    style: TextStyle(
                      color: Colors.blue,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),

              // wrap a floating action btn in a padding to remove it from the edge of screen
              // aligned the button in the center
              Padding(
                padding: EdgeInsets.all(40.0),
                child: Center(
                  // button simply routes to the home page/dashboard of our app onClick
                  child: FloatingActionButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/home');
                    },
                    backgroundColor: Colors.tealAccent[700],
                    child: Icon(
                      Icons.login_rounded,
                      color: Colors.black87,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
