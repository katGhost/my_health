import 'package:flutter/material.dart';

class ExercisesTile extends StatelessWidget {
  final String title;
  final icon;

  const ExercisesTile({
    Key? key,
    required this.title,
    required this.icon,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 25.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0),
          color: Colors.black54,
        ),
        width: 200,
        padding: EdgeInsets.all(15),
        child: Column(
          children: [
            Icon(
              icon,
              size: 90,
              color: Colors.tealAccent[400],
            ),

            SizedBox(
              height: 20,
            ),

            // Title
            Text(
              title,
              style: TextStyle(
                color: Colors.tealAccent,
                fontSize: 18,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.5,
              ),
            ),

            SizedBox(
              height: 20,
            ),
            Text(
              'This exercise has been adjusted according to your activties, and modified just for you. Add exercise?',
              style: TextStyle(
                color: Colors.grey[400],
                fontSize: 16,
                letterSpacing: 1.0,
              ),
            ),

            SizedBox(
              height: 25,
            ),
            Container(
              padding: EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                color: Colors.teal,
                borderRadius: BorderRadius.circular(12.0),
              ),
              child: Center(
                child: Text(
                  'Add',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
