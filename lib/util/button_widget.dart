import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  final String text;
  final VoidCallback onClicked;

  const ButtonWidget({
    Key? key,
    required this.text,
    required this.onClicked,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => FlatButton(
        padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 20),
        color: Colors.black,
        onPressed: onClicked,
        focusColor: Colors.black54,
        hoverColor: Colors.black54,
        child: Text(
          text,
          style: TextStyle(
            color: Colors.tealAccent[400],
            fontSize: 20,
            letterSpacing: 1.0,
          ),
        ),
      );
}
