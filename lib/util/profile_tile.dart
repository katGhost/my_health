import 'package:flutter/material.dart';

class ProfileTile extends StatelessWidget {
  final String header;
  final icon;

  const ProfileTile({
    Key? key,
    required this.header,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Container(
        width: 200,
        height: 100,
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: Column(
          children: [
            Icon(
              icon,
              color: Colors.teal,
              size: 30,
            ),
            SizedBox(
              height: 25,
            ),
            Text(
              header,
              style: TextStyle(
                color: Colors.teal,
                fontWeight: FontWeight.bold,
                fontSize: 18,
                letterSpacing: 1.0,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Explore more? click below',
              style: TextStyle(
                color: Colors.grey[700],
                fontSize: 14,
                letterSpacing: 1.0,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                color: Colors.tealAccent[700],
                borderRadius: BorderRadius.circular(12.0),
              ),
              child: Center(
                child: Text(
                  'Enter',
                  style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
