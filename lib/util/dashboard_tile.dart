import 'package:flutter/material.dart';

class DashBoardTile extends StatelessWidget {
  final String title;
  final String sub;
  final VoidCallback onClicked;
  final icon;
  final color;

  const DashBoardTile(
      {Key? key,
      required this.title,
      required this.sub,
      required this.onClicked,
      required this.icon,
      required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 200,
        height: 100,
        padding: EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            // title
            Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.grey[300],
                fontSize: 16,
                letterSpacing: 1.5,
              ),
            ),

            // subtitle
            Text(
              sub,
              style: TextStyle(
                color: Colors.grey,
                fontSize: 14,
              ),
            ),

            // button to navigate
            FlatButton(
              padding: EdgeInsets.all(20.0),
              onPressed: onClicked,
              child: Icon(
                icon,
                color: Colors.white,
              ),
              color: color,
            ),
          ],
        ),
      ),
    );
  }
}
