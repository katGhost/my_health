import 'package:flutter/material.dart';
import 'package:gallery/pages/motivation.dart';
import 'package:gallery/pages/stopwatch.dart';
import 'package:gallery/pages/exercise.dart';
import 'package:gallery/pages/login.dart';
import 'package:gallery/pages/home.dart';
import 'package:gallery/pages/profile.dart';

void main() => runApp(MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/login',
        routes: {
          '/': (context) => Login(),
          '/home': (context) => Home(),
          '/profile': (context) => MyProfile(),
          '/exercise': (context) => Exercises(),
          '/stopwatch': (context) => CountDownTimer(),
          '/motivation': (context) => MotivationPage(),
        }));
